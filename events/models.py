from __future__ import unicode_literals

from django.conf import settings
from django.db import models

class Event(models.Model):
    """Defines a single event."""

    class Meta:
        ordering = ['-created_at']

    activity = models.CharField(max_length=200)
    image = models.URLField()
    description = models.TextField()
    location = models.TextField()
    start_at = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)

    def __unicode__(self):
        return self.activity

    def save(self, *args, **kwargs):
        super(Event, self).save(*args, **kwargs)


