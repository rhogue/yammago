from django.shortcuts import get_object_or_404, render

from .models import Event

def index(request):
    latest_event_list = Event.objects.order_by('-created_at')[:5] #get last 5 events posted to page
    context = {
        'latest_event_list': latest_event_list,
    }
    return render(request, 'events/index.html', context)

def detail(request, event_id):
    e = get_object_or_404(Event, pk=event_id) #event is the class, event id is the identifier for kv

    return render(request, 'events/detail.html', {'event': e})



