#this file is a list of third party apps to be installed. These cannot go in dev or prod 
#becuase google loads those apps from their server (they are builtins to GAE).
#These apps should be installed into 'lib' using pip with the -t argument:
#     pip install -r third-party.txt -t lib

# python-social-auth
social-auth-app-django
